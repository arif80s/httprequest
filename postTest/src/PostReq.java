
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author abra
 */
//receive json string and send post request to localhost
public class PostReq 
{

    public static void main(String args[]) throws Exception 
    {
        
        URL url = new URL("http://localhost:8080/senddata");
        
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setConnectTimeout(5000);//5 secs
        connection.setReadTimeout(5000);//5 secs
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        connection.setRequestProperty("Content-Type", "application/json");
        
        OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
 
        Jackson2Example jsonStaffObject = new Jackson2Example();
        
//      call run function of jacksion2Example
        out.write(jsonStaffObject.run());
        out.flush();
        out.close();

        int res = connection.getResponseCode();

        System.out.println(res);

        InputStream is = connection.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line = null;
        
        while ((line = br.readLine()) != null) 
        {
            System.out.println(line);
        }
        
        connection.disconnect();

    }
}
